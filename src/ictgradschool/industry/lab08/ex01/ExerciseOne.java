package ictgradschool.industry.lab08.ex01;

import jdk.nashorn.internal.ir.WhileNode;

import javax.sound.sampled.Line;
import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }


    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        try {
            FileReader reader = new FileReader("input2.txt");

            // Read a single character from the file
            int lettere = reader.read();
            // Loop until that character is -1
            while (lettere != -1) {
                // If that character is 'e' then incrament e
                if (lettere == 'e' || lettere == 'E') {
                    numE++;
                }
                // increment the counter for how many times we've looped
                total++;
                // read another character from the file
                lettere = reader.read();
                // end loop
            }


        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        try (BufferedReader reader = new BufferedReader(new FileReader("input2.txt"))) {

            String line = null;
            while ((line = reader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    // If that character is 'e' then incrament e
                    if (line.charAt(i) == 'e' || line.charAt(i) == 'E') {
                        numE++;
                    }
                    // increment the counter for how many times we've looped
                    total++;
                    // end loop
                }
            }

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
