package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner

        File myFile = new File(fileName);

        try (Scanner scanner = new Scanner(myFile)) {

            int i = 0;
            scanner.useDelimiter(",| \\r\\n");
            Movie[] movie = new Movie[scanner.nextInt()];
            while (i < movie.length) {
                String name = scanner.next();
                int year = scanner.nextInt();
                int lengthInMinutes = scanner.nextInt();
                String director = scanner.next();
                movie[i] = new Movie(name, year, lengthInMinutes, director);
                i++;
            }
            return movie;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
