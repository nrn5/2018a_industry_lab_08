package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        try (Scanner scanner = new Scanner(new File(fileName))) {

            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
